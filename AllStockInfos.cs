﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReadCsv
{

    class AllStockInfos : InfoValue
    {
        private string stockID;
        private string stockName;
        private List<InfoValue> allStockInfos;
        private List<StockSum> stockSums;
        private List<CountTop50> buyCellOver;
        public AllStockInfos()
        {
            allStockInfos = new List<InfoValue>();
            stockSums = new List<StockSum>();
            buyCellOver = new List<CountTop50>();
        }

        public List<InfoValue> GetAllStockInfos()
        {
            return allStockInfos;
        }
        public void SetAllStockInfos(InfoValue stockInfos)
        {
            allStockInfos.Add(stockInfos);
        }


        //獲得StockSum的List
        public void GetAllStockSums()
        {
            for (int i = 0; i < allStockInfos.Count; i++)
            {
                stockSums.Add(allStockInfos[i].GetStockSum());
            }
        }
        public List<StockSum> GetStockSums()
        {
            return stockSums;
        }
        public void GetAllBuyCellOvers()
        {
            for (int i = 0; i < allStockInfos.Count; i++)
            {
                for (int j = 0; j < allStockInfos[i].GetBrokerDistinct().Count; j++)
                {
                    buyCellOver.Add(allStockInfos[i].GetBrokerDistinct().ElementAt(j));
                }

            }
        }
        public List<CountTop50> GetBrokerDistinct(Dictionary<string,InfoValue> stockDictionary)
        {
           
           GetAllBuyCellOvers();//改
            return buyCellOver;
        }

        //以下是單一個StockList的

        public void SetTheStockInfos(StockInfo stockInfo)
        {
            throw new NotImplementedException();
        }

        public List<StockInfo> TheStockInfos()
        {
            List<StockInfo> stocks = new List<StockInfo>();
            for (int i = 0; i < allStockInfos.Count; i++)
            {
                for (int j = 0; j < allStockInfos[i].TheStockInfos().Count; j++)
                {
                    stocks.Add(allStockInfos[i].TheStockInfos().ElementAt(j));
                }

            }
            return stocks;
        }



        public string TheStockID()
        {
            return stockID;
        }

        public string TheStockName()
        {
            return stockName;
        }

        List<StockInfos> InfoValue.GetAllStockInfos()
        {
            throw new NotImplementedException();
        }

        public void CalculateStockSum()
        {

        }



        public StockSum GetStockSum()
        {
            throw new NotImplementedException();
        }

        public List<CountTop50> GetBrokerDistinct()
        {if(buyCellOver.Count == 0)
            {
                GetAllBuyCellOvers();
            }
            return buyCellOver;
            
        }




        //public void SetAllStockInfos(StockInfo stockInfo)
        //{
        //    StockInfos stockinfos = new StockInfos(stockInfo.StockID);


        //    allStockInfos.Add(new StockInfos());

        //}
    }
}
