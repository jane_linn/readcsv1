﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadCsv
{
    interface InfoValue
    {
        string TheStockID();
        List<StockInfo> TheStockInfos();
       string TheStockName();
        void SetTheStockInfos(StockInfo stockInfo);
        List<StockInfos> GetAllStockInfos();
        void SetAllStockInfos(InfoValue stockInfos);
        void CalculateStockSum();
        List<StockSum> GetStockSums();
        StockSum GetStockSum();
        void GetAllStockSums();
        List<CountTop50> GetBrokerDistinct(Dictionary<string, InfoValue> stockDictionary);
        List<CountTop50> GetBrokerDistinct();
        void GetAllBuyCellOvers();
    }
}
