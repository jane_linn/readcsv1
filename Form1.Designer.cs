﻿namespace ReadCsv
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.tx_file_root = new System.Windows.Forms.TextBox();
            this.cob_comboBox = new System.Windows.Forms.ComboBox();
            this.btn_choose_file = new System.Windows.Forms.Button();
            this.btn_search = new System.Windows.Forms.Button();
            this.btn_top50 = new System.Windows.Forms.Button();
            this.tb_time = new System.Windows.Forms.TextBox();
            this.dgv_rho_data = new System.Windows.Forms.DataGridView();
            this.dgv_search_result = new System.Windows.Forms.DataGridView();
            this.dgv_top50_result = new System.Windows.Forms.DataGridView();
            this.label_reading = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_rho_data)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_search_result)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_top50_result)).BeginInit();
            this.SuspendLayout();
            // 
            // tx_file_root
            // 
            this.tx_file_root.Location = new System.Drawing.Point(28, 41);
            this.tx_file_root.Name = "tx_file_root";
            this.tx_file_root.Size = new System.Drawing.Size(190, 22);
            this.tx_file_root.TabIndex = 0;
            // 
            // cob_comboBox
            // 
            this.cob_comboBox.FormattingEnabled = true;
            this.cob_comboBox.Location = new System.Drawing.Point(28, 83);
            this.cob_comboBox.Name = "cob_comboBox";
            this.cob_comboBox.Size = new System.Drawing.Size(190, 20);
            this.cob_comboBox.TabIndex = 1;
            // 
            // btn_choose_file
            // 
            this.btn_choose_file.Location = new System.Drawing.Point(245, 41);
            this.btn_choose_file.Name = "btn_choose_file";
            this.btn_choose_file.Size = new System.Drawing.Size(75, 23);
            this.btn_choose_file.TabIndex = 2;
            this.btn_choose_file.Text = "選取檔案路徑";
            this.btn_choose_file.UseVisualStyleBackColor = true;
            this.btn_choose_file.Click += new System.EventHandler(this.btn_choose_file_Click);
            // 
            // btn_search
            // 
            this.btn_search.Location = new System.Drawing.Point(245, 79);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(75, 23);
            this.btn_search.TabIndex = 3;
            this.btn_search.Text = "查詢";
            this.btn_search.UseVisualStyleBackColor = true;
            this.btn_search.Click += new System.EventHandler(this.Btn_search_Click);
            // 
            // btn_top50
            // 
            this.btn_top50.Location = new System.Drawing.Point(336, 78);
            this.btn_top50.Name = "btn_top50";
            this.btn_top50.Size = new System.Drawing.Size(75, 23);
            this.btn_top50.TabIndex = 4;
            this.btn_top50.Text = "買賣超前50";
            this.btn_top50.UseVisualStyleBackColor = true;
            this.btn_top50.Click += new System.EventHandler(this.Btn_top50_Click);
            // 
            // tb_time
            // 
            this.tb_time.Location = new System.Drawing.Point(673, 41);
            this.tb_time.Multiline = true;
            this.tb_time.Name = "tb_time";
            this.tb_time.Size = new System.Drawing.Size(352, 60);
            this.tb_time.TabIndex = 8;
            // 
            // dgv_rho_data
            // 
            this.dgv_rho_data.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_rho_data.Location = new System.Drawing.Point(28, 126);
            this.dgv_rho_data.Name = "dgv_rho_data";
            this.dgv_rho_data.RowHeadersWidth = 62;
            this.dgv_rho_data.RowTemplate.Height = 24;
            this.dgv_rho_data.Size = new System.Drawing.Size(617, 162);
            this.dgv_rho_data.TabIndex = 11;
            // 
            // dgv_search_result
            // 
            this.dgv_search_result.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_search_result.Location = new System.Drawing.Point(28, 310);
            this.dgv_search_result.Name = "dgv_search_result";
            this.dgv_search_result.RowHeadersWidth = 62;
            this.dgv_search_result.RowTemplate.Height = 24;
            this.dgv_search_result.Size = new System.Drawing.Size(617, 135);
            this.dgv_search_result.TabIndex = 12;
            // 
            // dgv_top50_result
            // 
            this.dgv_top50_result.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_top50_result.Location = new System.Drawing.Point(673, 126);
            this.dgv_top50_result.Name = "dgv_top50_result";
            this.dgv_top50_result.RowHeadersWidth = 62;
            this.dgv_top50_result.RowTemplate.Height = 24;
            this.dgv_top50_result.Size = new System.Drawing.Size(352, 319);
            this.dgv_top50_result.TabIndex = 13;
            // 
            // label_reading
            // 
            this.label_reading.AutoSize = true;
            this.label_reading.Location = new System.Drawing.Point(250, 20);
            this.label_reading.Name = "label_reading";
            this.label_reading.Size = new System.Drawing.Size(0, 12);
            this.label_reading.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(673, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 15;
            this.label1.Text = "花費時間";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1041, 466);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label_reading);
            this.Controls.Add(this.dgv_top50_result);
            this.Controls.Add(this.dgv_search_result);
            this.Controls.Add(this.dgv_rho_data);
            this.Controls.Add(this.tb_time);
            this.Controls.Add(this.btn_top50);
            this.Controls.Add(this.btn_search);
            this.Controls.Add(this.btn_choose_file);
            this.Controls.Add(this.cob_comboBox);
            this.Controls.Add(this.tx_file_root);
            this.Name = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_rho_data)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_search_result)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_top50_result)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tx_file_root;
        private System.Windows.Forms.ComboBox cob_comboBox;
        private System.Windows.Forms.Button btn_choose_file;
        private System.Windows.Forms.Button btn_search;
        private System.Windows.Forms.Button btn_top50;
        private System.Windows.Forms.TextBox tb_time;
        private System.Windows.Forms.DataGridView dgv_rho_data;
        private System.Windows.Forms.DataGridView dgv_search_result;
        private System.Windows.Forms.DataGridView dgv_top50_result;
        private System.Windows.Forms.Label label_reading;
        private System.Windows.Forms.Label label1;
    }
}

