﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReadCsv
{
    public partial class Form1 : Form
    {
        //計算時間
        private Stopwatch stopwatch;
        private string strTime;
        //stringbuilder
        private StringBuilder sb;
        //讀檔進來的所有資料(rho data)
        private List<StockInfo> rhoStockInfos;
        //顯示在comboBox且依據StockID存List<Info>
        private Dictionary<string, List<StockInfo>> stockDictionary;
        //用來判斷comboBox上的字有沒有改變
        private List<MyComboBoxItem> comboBoxTextList;
        private string comboBoxText;
        //查詢出來的結果(必要嗎?)
        List<InfoValue> selectInfos;
        //有重新讀檔嗎
        private string rootText;



        public Form1()
        {
            InitializeComponent();
            rootText = "";
            sb = new StringBuilder();
            stopwatch = new Stopwatch();
            selectInfos = new List<InfoValue>();
            stockDictionary = new Dictionary<string, List<StockInfo>>();//key:ID ; value: 股票號碼(可以不只一個)
        }

        /*
        * 選取要讀的csv檔後將資料顯示在datagridview中並且建立comboBox
        */
        private void btn_choose_file_Click(object sender, EventArgs e)
        {
            string filePath = "";
            OpenFileDialog opnFile = new OpenFileDialog();  //按按鈕才會new
            //設定屬性
            opnFile.InitialDirectory = "c:/";
            opnFile.Filter = "txt file (*.csv)|*.csv";
            opnFile.FilterIndex = 2;//只能讀csv檔(要防呆)

            //當確認鍵被按(要例外處理)
            if (opnFile.ShowDialog() == DialogResult.OK)
            {
                //清空舊的資料
                dgv_rho_data.DataSource = null;
                dgv_search_result.DataSource = null;
                dgv_top50_result.DataSource = null;
                dgv_rho_data.Rows.Clear();
                dgv_search_result.Rows.Clear();
                dgv_top50_result.Rows.Clear();
                
                //取得選取的檔案路徑(show在textBox裡)
                filePath = opnFile.FileName;
                tx_file_root.Text = filePath;
                tx_file_root.Update();
                
                stopwatch.Restart();
                //讀檔method
                ReadCsv(filePath);
                //顯示在datagridview
                dgv_rho_data.DataSource = new BindingSource(rhoStockInfos, null);
                dgv_rho_data.Update();
                StopWatch("讀檔完成: ");
                label_reading.Text = "檔案讀取完成";
                label_reading.Update();
               
                stopwatch.Restart();
                //建立comboBox
                cob_comboBox.DisplayMember = "Text";
                cob_comboBox.ValueMember = "StockID";
                cob_comboBox.DataSource = comboBoxTextList;
                StopWatch("comboBox 建立時間 - ");
                

            }
            //建立dictionary
            //把全部照順序放進ALL(待修!)
            CreateDictionary();
        }

        /*
         * 讀檔+ comboBoxList建立
         * 使用streamReader讀進一個csv檔
         */
        private void ReadCsv(string filePath)
        {
            if (filePath != null)
            {
                rhoStockInfos = new List<StockInfo>();
                comboBoxTextList = new List<MyComboBoxItem>();
                comboBoxTextList.Add(new MyComboBoxItem {Text = "All",StockID =""}) ;
                string allId = "";
                label_reading.Text = "檔案讀取中";
                label_reading.Update();
                //用streamreader讀取檔案
                using (StreamReader reader = new StreamReader(filePath, System.Text.Encoding.Default))
                {
                    //紀錄一整行
                    string lines = "";
                    //每一行的分段
                    string[] datas = null;
                    //注意第一行不是data
                    int row = 0;
                    //開始逐行讀取
                    while ((lines = reader.ReadLine()) != null)
                    {
                        if (row > 0)
                        {
                            datas = lines.Split(',');
                            StockInfo stockInfo = new StockInfo
                            {
                                DealDate = datas[0],
                                StockID = datas[1],
                                StockName = datas[2],
                                SecBrokerID = datas[3],
                                SecBrokerName = datas[4],
                                Price = Convert.ToDouble(datas[5]),
                                BuyQty = Convert.ToInt32(datas[6]),
                                CellQty = Convert.ToInt32(datas[7])
                            };
                            rhoStockInfos.Add(stockInfo);
                            //key為顯示的 value是StockID
                            comboBoxTextList.Add(new MyComboBoxItem { Text = (stockInfo.StockID + " - " + stockInfo.StockName), StockID = stockInfo.StockID });
                            allId += stockInfo.StockID + ",";
                        }
                        row += 1;
                    }
                }
                comboBoxTextList[0].StockID = allId;//最後有一個，喔!
                comboBoxTextList = comboBoxTextList.Distinct().ToList();
            }
        }

        /*
         * 建立combobox
         * 依照所有的資料建立一個Dictionary並顯示在comboBox
         */
        private void CreateDictionary()
        {
            stopwatch.Restart();
            //建立Dictionary<string,List<StockInfo>>
            //stockDictionary = rhoStockInfos.ToDictionary(key => key.SecBrokerID, value => List < StockInfo > value);
            stockDictionary = rhoStockInfos.GroupBy(id => id.StockID).ToDictionary(key => key.Key, list => list.ToList());

            //一筆一筆建Dic
            //foreach (StockInfo info in rhoStockInfos)
            //{
            //    InfoValue value = null;
            //    if (!stockDictionary.TryGetValue(info.StockID, out value))
            //    {
            //        stockDictionary.Add(info.StockID, new StockInfos(info.StockID, info.StockName));

            //        cob_comboBox.Items.Add(info.StockID + " - " + info.StockName);//comboBox放入items
            //    }
            //    stockDictionary[info.StockID].SetTheStockInfos(info);
            //}

        
            StopWatch("Dic 建立時間 - ");

        }

        ///*
        // * 查詢btn
        // * 依照comboBox上的資料取得StockID
        // */
        private void Btn_search_Click(object sender, EventArgs e)
        {
            //    stopwatch.Restart();
            //    if (cob_comboBox.Text != null)
            //    {

            //        GetSelectInfos();
            //        if (selectInfos.Count == 0 || selectInfos == null)
            //        {
            //            MessageBox.Show("指定的索引鍵不在字典中");
            //            stopwatch.Stop();
            //            return;
            //        }
            //        ShowSearchResult(selectInfos);
            //        StopWatch("查詢時間 - ");
            //    }
            //    stopwatch.Stop();
        }

        ///*
        //* 判斷comboBox上的字串對應到誰(哪個StockID)的資料包
        //*/
        //private void GetSelectInfos()
        //{
        //    selectInfos = null;
        //    selectInfos = new List<InfoValue>();
        //    comboBoxText = cob_comboBox.Text;
        //    //list的內容是Dictionary的value(是StockInfos 或 AllStockInfos ; 是同一個StockID的所有Info的List)
        //    string[] typeKey;
        //    //用打的
        //    if (cob_comboBox.SelectedItem == null)
        //    {
        //        typeKey = cob_comboBox.Text.Split(',');
        //    }
        //    else
        //    {
        //        //用選的
        //        string[] select = cob_comboBox.Text.Split('-').First().Split(' ');
        //        typeKey = new string[1];
        //        typeKey[0] = (select[0]);

        //    }
        //    for (int i = 0; i < typeKey.Length; i++)
        //    {
        //        string value = null;
        //        if (stockDictionary.TryGetValue(typeKey[i], out value))
        //        {
        //            selectInfos.Add(value);
        //        }
        //    }
        //    //打了兩個一樣的???????????會出現兩次
        //    //text空的????????????????指定的索引不再字典中
        //    //All和ID-->只顯示ID的???????????????
        //    //ID+KEY都會查

        //}
        ///*
        // * show on datagridview
        // * 依照comboBox上的資料顯示查詢結果
        // * 第一個表格顯示InfoValue裡所有List的資料
        // */
        //private void ShowSearchResult(List<InfoValue> selectInfos)
        //{
        //    stockDictionary["All"].GetAllStockSums();
        //    List<StockInfo> show = new List<StockInfo>();

        //    //計算數值
        //    List<StockSum> sumInfos = new List<StockSum>();
        //    if (selectInfos.Count > 1)
        //    {
        //        for (int i = 0; i < selectInfos.Count; i++)
        //        {
        //            for (int m = 0; m < selectInfos[i].TheStockInfos().Count; m++)
        //            {
        //                show.Add(selectInfos[i].TheStockInfos().ElementAt(m));
        //            }
        //            for (int j = 0; j < selectInfos[i].GetStockSums().Count; j++)
        //            {
        //                sumInfos.Add(selectInfos[i].GetStockSums().ElementAt(j));
        //            }
        //        }
        //    }
        //    else if (selectInfos.Count == 1)
        //    {
        //        show = selectInfos[0].TheStockInfos();
        //        sumInfos = selectInfos[0].GetStockSums();

        //    }
        //    //填入第一個表格
        //    dgv_rho_data.DataSource = null;
        //    dgv_rho_data.DataSource = new BindingSource(show, null);
        //    dgv_rho_data.Update();
        //    //填入第二個表格
        //    dgv_search_result.DataSource = null;
        //    dgv_search_result.DataSource = sumInfos;
        //    dgv_search_result.Update();
        //}

        ///*
        // * 依照comboBox上的資料顯示買賣超前50
        // */
        private void Btn_top50_Click(object sender, EventArgs e)
        {

            //    stopwatch.Restart();
            //    if (cob_comboBox.Text != null)
            //    {
            //        //把每個都先算出來
            //        //for (int i = 1; i < stockDictionary.Values.Count; i++)
            //        //{
            //        //    InfoValue n = stockDictionary.ElementAt(i).Value;
            //        //    n.GetAllBuyCellOvers();
            //        //}

            //        GetSelectInfos();
            //        if (selectInfos.Count == 0 || selectInfos == null)
            //        {
            //            MessageBox.Show("指定的索引鍵不在字典中");
            //            stopwatch.Stop();
            //            return;
            //        }
            //        ShowTop50Result(selectInfos);
            //        StopWatch("top50 - ");
            //    }
            //    stopwatch.Stop();
        }
        //private void ShowTop50Result(List<InfoValue> selectInfos)
        //{
        //    List<CountTop50> sortList = new List<CountTop50>();
        //    if (selectInfos.Count > 1)
        //    {
        //        for (int i = 0; i < selectInfos.Count; i++)
        //        {
        //            for (int j = 0; j < selectInfos[i].GetBrokerDistinct().Count; j++)
        //                sortList.Add(selectInfos[i].GetBrokerDistinct().ElementAt(j));
        //        }
        //    }
        //    else
        //    {

        //        sortList = selectInfos[0].GetBrokerDistinct();
        //    }

        //    dgv_top50_result.DataSource = null;
        //    dgv_top50_result.Rows.Clear();
        //    dgv_top50_result.DataSource = new BindingSource(sortList, null);
        //    dgv_top50_result.Update();
        //}
        ///*
        // * stopwatch stop ，傳入要顯示的字串
        // */
        public void StopWatch(string msg)
        {
            stopwatch.Stop();
            strTime = "";
            TimeSpan time = stopwatch.Elapsed;
            strTime = String.Format("{0:00}:{1:00}:{2:00}:{3:000}", time.Hours, time.Minutes, time.Seconds, time.Milliseconds);
            tb_time.AppendText(msg + strTime + "\r\n");
        }

        //private StringBuilder CombineString(string a, string b, string c)
        //{
        //    sb.Append(a);
        //    sb.Append(b);
        //    sb.Append(c);
        //    return sb;
        //}


    }
}

























//linQtoCsv
//label_reading.Text = "檔案讀取中";
//    label_reading.Update();
//    //使用linQtoCsv套件
//    stockInfos = new List<StockInfo>();
//    StockInfo value;
//    CsvFileDescription inputFileDescription = new CsvFileDescription
//    {
//        SeparatorChar = ',',
//        FirstLineHasColumnNames = true,
//         EnforceCsvColumnAttribute = true,
//        TextEncoding = Encoding.Default
//    };
//    stopwatch.Restart();
//    CsvContext cc = new CsvContext();
//    return cc.Read<StockInfo>(filePath, inputFileDescription).ToList();

//using (FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
//{
//    using (StreamReader reader = new StreamReader(fileStream, Encoding.Default))
//    {
//        using (CsvReader csv = new CsvReader(reader, CultureInfo.InvariantCulture))
//        {
//            csv.Configuration.HasHeaderRecord = true;
//            csv.Configuration.HeaderValidated = null;
//            csv.Configuration.MissingFieldFound = null;
//            csv.Configuration.RegisterClassMap<MyMap>();
//            csv.Read();
//            return csv.GetRecords<StockInfo>().ToList();
//        }
//    }
//}


//streamreader
//DataTable dt = new DataTable();
//if (filePath != "")
//{
//    label_reading.Text = "檔案讀取中";
//    label_reading.Update();
//    //用streamreader讀取檔案
//    //Stream fileStream = opnFile.OpenFile();
//    //try
//    //{
//    using (StreamReader reader = new StreamReader(filePath, System.Text.Encoding.Default))
//    {
//        //初始
//        dt.Columns.Add();
//        dt.Columns.Add();
//        dt.Columns.Add();
//        dt.Columns.Add();
//        dt.Columns.Add();
//        dt.Columns.Add();
//        dt.Columns.Add();
//        dt.Columns.Add();
//        //紀錄一整行
//        string lines = "";

//        //每一行的分段
//        string[] datas = null;
//        //表示讀到第幾列了
//        int row = 0;
//        //注意第一行不是data
//        //開始逐行讀取
//        while ((lines = reader.ReadLine()) != null)
//        {
//            if (row > 0)
//            {
//                datas = lines.Split(',');
//                DataRow newRow = dt.NewRow();
//                for (int i = 0; i < 8; i++)
//                {
//                    newRow[i] = datas[i];
//                }
//                dt.Rows.Add(newRow);

//            }
//            row += 1;
//        }



//    }
//    return dt;
//    //}
//    //catch (ArgumentException ex)
//    //{
//    //    MessageBox.Show("索引在陣列的界線之外");
//    //}
//}

//newRow["DealDate"] = DateTime.ParseExact(datas[0],"yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
//newRow["StocksID"] = datas[1];
//newRow["StocksName"] = datas[2];
//newRow["SecBrokerID"] = datas[3];
//newRow["SecBrokerName"] = datas[4];
//newRow["Price"] = Convert.ToDouble(datas[5]);
//newRow["BuyQty"] = Convert.ToInt32(datas[6]);
//newRow["CellQty"] = Convert.ToInt32(datas[7]);

//int totalBuy = dgv_rho_data.Rows.Cast<DataGridViewRow>()
//    .Sum(t => Convert.ToInt32(t.Cells["Column6"].Value));
//int totalCell = dgv_rho_data.Rows.Cast<DataGridViewRow>()
//    .Sum(t => Convert.ToInt32(t.Cells["Column7"].Value));

//int countBroker = dgv_rho_data.Rows.Cast<DataGridViewRow>()
//    .Count(t => Convert.ToInt32(t.Cells["Column6"].Value));


//Dictionary 
//InfoValue value = null;
//    if (!stockDictionary.TryGetValue(datas[1], out value))
//    {
//        stockDictionary.Add(datas[1], new StockInfos(datas[1], datas[2]));
//    }
//    stockDictionary[datas[1]].SetTheStockInfos(stockInfo);