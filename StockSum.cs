﻿
namespace ReadCsv
{
    class StockSum
    {
        public string StockID { get; set; }
        public string StockName { get; set; }
        public int BuyTotal { get; set; }
        public int CellTotal { get; set; }
        public double AvgPrice { get; set; }
        public int BuyCellOver { get; set; }
        public int SecBrokerCnt { get; set; }
        
    }
}
