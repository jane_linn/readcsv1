﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadCsv
{

    class StockInfos : InfoValue
    {
        //這是哪一個StockID的資訊包
        private string theStockID;
        private string theStockName;
        private string theBrockerName;
        //所有這個StockID的資訊List
        private List<StockInfo> theStockInfos;
        private StockSum sumInfos;
        //top50List
        private List<CountTop50> top50 = new List<CountTop50>();
        public StockInfos(string theStockID, string theStockName)
        {
            this.theStockID = theStockID;
            this.theStockName = theStockName;
            theStockInfos = new List<StockInfo>();
            sumInfos = new StockSum();
            top50 = new List<CountTop50>();
        }

        public string TheStockID()
        {
            return theStockID;
        }
        public string TheStockName()
        {
            return theStockName;
        }
        public StockSum GetStockSum()
        {
            return sumInfos;
        }
        //把資訊一個一個放進來
        public void SetTheStockInfos(StockInfo stockInfo)
        {
            theStockInfos.Add(stockInfo);
        }

        //計算StockSum
        public void CalculateStockSum()
        {
            List<string> brokerDistinct = new List<string>();
            for (int i = 0; i < theStockInfos.Count; i++)
            {
                sumInfos.BuyTotal += theStockInfos[i].BuyQty;
                sumInfos.CellTotal += theStockInfos[i].CellQty;
                sumInfos.AvgPrice += theStockInfos[i].Price;
                if (!brokerDistinct.Contains(theStockInfos[i].SecBrokerID))
                {
                    brokerDistinct.Add(theStockInfos[i].SecBrokerID);
                }
            }
            sumInfos.StockID = theStockID;
            sumInfos.StockName = theStockName;
            sumInfos.BuyCellOver = sumInfos.BuyTotal - sumInfos.CellTotal;
            sumInfos.SecBrokerCnt = brokerDistinct.Count;
            sumInfos.AvgPrice = sumInfos.AvgPrice / theStockInfos.Count;
        }

        public List<StockSum> GetStockSums()
        {
            List<StockSum> stockSums = new List<StockSum>();
            stockSums.Add(sumInfos);
            return stockSums;
        }

        //依照券商的名字groupby並sum buytotal 和 celltotal 回傳一個額外的list 
        public List<CountTop50> GetBrokerDistinct()
        {
            if (top50.Count==0)
            {
                GetAllBuyCellOvers();
            }

            return top50;
        }
        public void GetAllBuyCellOvers()
        {
            theBrockerName = theStockInfos[0].SecBrokerName;

            var resultD = (from p in theStockInfos

                           group p by p.SecBrokerID into g
                           select new
                           {
                               StockName = theStockName,
                               SecBrokerName = g.Select(p => p.SecBrokerName).First(),
                               BuyCellOver = g.Sum(p => p.BuyQty) - g.Sum(p => p.CellQty)
                           })
                          .OrderByDescending(x => x.BuyCellOver).Take(50);
            var resultA = (from p in theStockInfos.AsEnumerable()

                           group p by p.SecBrokerID into g
                           select new
                           {
                               StockName = theStockName,
                               SecBrokerName = g.Select(p => p.SecBrokerName).First().ToString(),
                               BuyCellOver = g.Sum(p => p.BuyQty) - g.Sum(p => p.CellQty)
                           })
                          .OrderBy(x => x.BuyCellOver).Take(50);
            for (int i = 0; i < resultD.Count(); i++)
            {
                var r = resultD.ElementAt(i);
                if (r != null && Convert.ToInt32(r.BuyCellOver) > 0)
                {
                    CountTop50 top = new CountTop50();
                    top.BuyCellOver = Convert.ToInt32(r.BuyCellOver);
                    top.StockName = Convert.ToString(r.StockName);
                    top.SecBrokerName = Convert.ToString(r.SecBrokerName);
                    top50.Add(top);
                }

            }
            for (int i = 0; i < resultA.Count(); i++)
            {
                var r = resultA.ElementAt(i);
                if (r != null && Convert.ToInt32(r.BuyCellOver) < 0)
                {
                    CountTop50 top = new CountTop50();
                    top.BuyCellOver = Convert.ToInt32(r.BuyCellOver);
                    top.StockName = Convert.ToString(r.StockName);
                    top.SecBrokerName = Convert.ToString(r.SecBrokerName);
                    top50.Add(top);
                }

            }
            //foreach (var r in result)
            //{
            //    CountTop50 top = new CountTop50();
            //    top.BuyCellOver = Convert.ToInt32(r.BuyCellOver);
            //    top.StockName = Convert.ToString(r.StockName);
            //    top.SecBrokerName = Convert.ToString(r.SecBrokerName);
            //    top50.Add(top);
            //}
            //if (top50.Count > 100)
            //{
            //    top50.RemoveRange(50, (top50.Count - 100));
            //}

        }

        //以下是all的
        public void SetAllStockInfos(InfoValue stockInfos)
        {
            throw new NotImplementedException();
        }




        public List<StockInfos> GetAllStockInfos()
        {
            throw new NotImplementedException();
        }

        public List<StockInfo> TheStockInfos()
        {
            return theStockInfos;
        }

        public void GetAllStockSums()
        {
            throw new NotImplementedException();
        }


        public List<CountTop50> GetBrokerDistinct(Dictionary<string, InfoValue> stockDictionary)
        {
            return top50;
        }
    }
}
