﻿
namespace ReadCsv
{
    class StockInfo
    {
        public string DealDate { get; set; }
        public string StockID { get; set; }
        public string StockName { get; set; }
        public string SecBrokerID { get; set; }
        public string SecBrokerName { get; set; }
        public double Price { get; set; }
        public int BuyQty { get; set; }
        public int CellQty { get; set; }

    }
}



//[Name("賣出張數")]
// [CsvColumn(Name = "賣出張數", FieldIndex = 7)]